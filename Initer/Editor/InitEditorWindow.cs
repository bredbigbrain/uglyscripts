﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.IMGUI.Controls;


public class InitEditorWindow : EditorWindow {

    public class Initable
    {
        public string name;
        public GameObject gameObject;
        public int priority = 0;
        public bool enabled = true;
        public IInitable initable;
    }

    public Vector2 MAX_SIZE = new Vector2(500, 4000);

    private Initer initer;
    private List<Initable> initables;

    private Vector2 scroll;
    private int[] rowWidths = new int[5] { 75, 130, 110, 105, 50 };

    [MenuItem("Ugly tools/Set init order")]
    public static void Open()
    {
        var window = GetWindow<InitEditorWindow>();

        window.maxSize = window.MAX_SIZE;
        window.minSize = new Vector2(window.MAX_SIZE.x, 125);

        window.Show();
    }

    private void OnGUI()
    {
        if (initer)
        {
            try
            {
                if (GUILayout.Button("Find Initables in scene", GUILayout.Height(25)))
                {
                    FindInitables();
                }

                if ((initables == null || initables.Count == 0) &&  initer.initablesGOs != null && initer.initablesGOs.Length > 0)
                {
                    GetInitablesFromIniter();
                }
                GUILayout.Space(10);
            }
            catch { }

            if (initables != null)
            {
                SortInitables();
                DisplayInitables();

                if (GUILayout.Button("Save", GUILayout.Height(25)))
                {
                    int enabledCount = 0;
                    for (int i = 0; i < initables.Count; i++)
                    {
                        if (initables[i].enabled)
                        {
                            enabledCount++;
                        }
                    }

                    initer.initablesGOs = new GameObject[enabledCount];

                    int j = 0;
                    for (int i = 0; i < initables.Count; i++)
                    {
                        if (initables[i].enabled)
                        {
                            initer.initablesGOs[j] = initables[i].gameObject;
                            j++;
                        }
                    }
                }

            }
        }
        else
        {
            OnIniterNull();
        }
    }

    void FindInitables()
    {
        initables = new List<Initable>();

        var gos = FindObjectsOfType(typeof(MonoBehaviour));
        IInitable initable;
        int p = 0;
        for (int i = 0; i < gos.Length; i++)
        {
            initable = gos[i] as IInitable;
            if (initable != null)
            {
                initables.Add(new Initable() { name = gos[i].name, gameObject = (gos[i] as MonoBehaviour).gameObject, priority = p, initable = initable });
                p++;
            }
        }
    }

    void GetInitablesFromIniter()
    {
        initables = new List<Initable>();

        IInitable initable;
        int p = 0;
        for (int i = 0; i < initer.initablesGOs.Length; i++)
        {
            if (initer.initablesGOs[i] != null)
            {
                var goMonos = initer.initablesGOs[i].GetComponents<MonoBehaviour>();
                for (int j = 0; j < goMonos.Length; j++)
                {
                    initable = goMonos[j] as IInitable;
                    if (initable != null)
                    {
                        initables.Add(new Initable() { name = initer.initablesGOs[i].name, gameObject = initer.initablesGOs[i], priority = p, initable = initable });
                        p++;
                    }
                }
            }
        }
    }

    void OnIniterNull()
    {
        var initers = FindObjectsOfType<Initer>();
        if (initers != null && initers.Length > 1)
        {
            GUILayout.Label("Two enabled initers in scene! Disable extra ones or set manualy:");
            initer = EditorGUILayout.ObjectField(initer, typeof(Initer), true) as Initer;
        }
        else
        {
            initer = FindObjectOfType<Initer>();
            if (initer == null)
            {
                GUILayout.Label("There is no enabled initers in scene!");
                if (GUILayout.Button("Create new initer"))
                {
                    GameObject go = new GameObject("Initer");
                    go.AddComponent<Initer>();
                }
            }
        }
    }

    void SortInitables()
    {
        Initable temp;
        
        for (int i = 1; i <= initables.Count; i++)
        {
            for (int j = 0; j < initables.Count - i; j++)
            {
                if (initables[j].priority > initables[j + 1].priority)
                {
                    temp = initables[j];
                    initables[j] = initables[j + 1];
                    initables[j + 1] = temp;
                }
            }
        }
    }

    void DisplayInitables()
    {
        scroll = GUILayout.BeginScrollView(scroll);

        GUILayout.BeginHorizontal(GUILayout.Width(MAX_SIZE.x - 10));

        bool enabled = initables[0].enabled;
        if (GUILayout.Button(enabled ? "Disable all" : "Enable all", GUILayout.Width(rowWidths[0])))
        {
            for (int i = 0; i < initables.Count; i++)
            {
                initables[i].enabled = !enabled;
            }
        }

        GUILayout.Label("Game object", GUILayout.Width(rowWidths[1]));
        GUILayout.Label("Skip frame awake", GUILayout.Width(rowWidths[2]));
        GUILayout.Label("Skip frame start", GUILayout.Width(rowWidths[3]));
        GUILayout.Label("Priority", GUILayout.Width(rowWidths[4]));

        GUILayout.EndHorizontal();
        Color color;
        for (int i = 0; i < initables.Count; i++)
        {
            color = i % 2 == 0 ? Color.white : Color.gray * 1.25f;
            GUI.backgroundColor = color;

            GUILayout.BeginHorizontal();

            initables[i].enabled = GUILayout.Toggle(initables[i].enabled, "", GUILayout.Width(rowWidths[0]));

            GUI.color = color;
            GUILayout.Label(initables[i].name, GUILayout.Width(rowWidths[1]));
            GUI.color = Color.white;

            initables[i].initable.SkipFrameBeforeAwake = GUILayout.Toggle(initables[i].initable.SkipFrameBeforeAwake, "", GUILayout.Width(rowWidths[2]));
            initables[i].initable.SkipFrameBeforeStart = GUILayout.Toggle(initables[i].initable.SkipFrameBeforeStart, "", GUILayout.Width(rowWidths[3]));

            initables[i].priority = EditorGUILayout.IntField(initables[i].priority, GUILayout.Width(rowWidths[4]));

            GUILayout.EndHorizontal();

            GUI.backgroundColor = Color.white;
        }

        GUILayout.EndScrollView();
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Initer : MonoBehaviour
{
    public float initDelayFrames = 10;

    public UnityEvent onInited;

    public GameObject[] initablesGOs;
    private IList<IInitable> initables;

    private void Awake()
    {
        SetInitables();

        if (initables == null || initables.Count == 0)
        {
            Destroy(gameObject);
        }
        else
        {
            StartCoroutine(DelayedInit());
        }
    }

    public void SetInitables()
    {
        initables = new List<IInitable>();

        IInitable initable;
        for (int i = 0; i < initablesGOs.Length; i++)
        {
            var goMonos = initablesGOs[i].GetComponents<MonoBehaviour>();
            for (int j = 0; j < goMonos.Length; j++)
            {
                initable = goMonos[j] as IInitable;
                if (initable != null)
                {
                    initables.Add(initable);
                }
            }
        }
    }

    IEnumerator DelayedInit()
    {
        for (int i = 0; i < initDelayFrames; i++)
        {
            yield return null;
        }

        for (int i = 0; i < initables.Count; i++)
        {
            if (initables[i].SkipFrameBeforeAwake)
            {
                yield return null;
            }
            initables[i].ManualAwake();
        }

        for (int i = 0; i < initables.Count; i++)
        {
            if (initables[i].SkipFrameBeforeStart)
            {
                yield return null;
            }
            initables[i].ManualStart();
        }

        if (onInited != null)
        {
            onInited.Invoke();
        }
        Destroy(gameObject);
    }
}

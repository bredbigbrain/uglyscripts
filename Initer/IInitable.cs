﻿public interface IInitable
{
    bool SkipFrameBeforeAwake { get;set; }
    bool SkipFrameBeforeStart { get;set; }

    void ManualAwake();
    void ManualStart();
}

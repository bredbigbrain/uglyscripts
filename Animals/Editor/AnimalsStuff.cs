﻿#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class AnimalsStuff : MonoBehaviour
{
    [MenuItem("Animals/Set mission mode")]
    public static void SetMissionMode()
    {
        PlayerPrefs.SetInt("MissionsMode", 1);
    }

    [MenuItem("Animals/Ugly tools/Set tutuorial gameGUI")]
    public static void SetTutorialGameGui()
    {
        bool tutorWasActive = false;
        Transform tutorialGui = null;
        RectTransform[] tutorialGameGui = null;
        foreach (var child in GameObject.Find("CanvasGame").GetComponentsInChildren<Transform>(true))
        {
            if (child.name == "TutorialPanel")
            {
                tutorWasActive = child.gameObject.activeSelf;
                tutorialGui = child.Find("GameGUI");
                child.gameObject.SetActive(true);
                tutorialGameGui = tutorialGui.GetComponentsInChildren<RectTransform>(true);
                break;
            }
        }

        var mainGameGui = GameObject.Find("CanvasGame").transform.Find("GameGUI").transform.GetComponentsInChildren<RectTransform>(true);

        foreach (var trans in tutorialGameGui)
        {
            foreach (var mainTrans in mainGameGui)
            {
                int depth = 5;
                if (trans.name != "GameGUI" && CheckNames(trans, mainTrans, ref depth))
                {
                    trans.gameObject.SetActive(mainTrans.gameObject.activeSelf);

                    trans.pivot = mainTrans.pivot;
                    trans.anchoredPosition = mainTrans.anchoredPosition;
                    trans.anchorMax = mainTrans.anchorMax;
                    trans.anchorMin = mainTrans.anchorMin;
                    trans.sizeDelta = mainTrans.sizeDelta;
                    trans.localPosition = mainTrans.localPosition;
                    trans.localRotation = mainTrans.localRotation;
                    trans.localScale = mainTrans.localScale;
                    trans.offsetMax = mainTrans.offsetMax;
                    trans.offsetMin = mainTrans.offsetMin;

                    var image = trans.GetComponent<Image>();
                    var imageMain = mainTrans.GetComponent<Image>();

                    if (image != null && imageMain != null)
                    {
                        image.sprite = imageMain.sprite;
                    }

                    var text = trans.GetComponent<Text>();
                    var textMain = mainTrans.GetComponent<Text>();

                    if (text != null && textMain != null)
                    {
                        text.text = textMain.text;
                        text.color = textMain.color;
                        text.font = textMain.font;
                        text.fontSize = textMain.fontSize;
                        text.fontStyle = textMain.fontStyle;
                        text.resizeTextForBestFit = textMain.resizeTextForBestFit;
                        text.alignment = textMain.alignment;
                    }
                }
            }
        }

        DestroyAll<Button>(tutorialGui);
        DestroyAll<UnityStandardAssets.CrossPlatformInput.Joystick>(tutorialGui);
        DestroyAll<AdditionalControls>(tutorialGui);
        DestroyAll<ControlsButton>(tutorialGui);
        DestroyAll<UnityEngine.EventSystems.EventTrigger>(tutorialGui);
        DestroyAll<StatBar>(tutorialGui);

        tutorialGui.parent.gameObject.SetActive(tutorWasActive);
    }

    static bool CheckNames(Transform trans, Transform mainTrans, ref int depth)
    {
        if (depth == 0)
        {
            return trans.name == mainTrans.name && trans.name != "GameGUI";
        }
        else
        {
            if (trans.name == mainTrans.name && trans.name != "GameGUI")
            {
                if (trans.parent.name == "GameGUI")
                {
                    return true;
                }
                depth--;
                return CheckNames(trans.parent, mainTrans.parent, ref depth);
            }
            else
            {
                return false;
            }
        }
    }

    static void DestroyAll<T>(Transform root)
    {
        var objects = root.GetComponentsInChildren<T>(true);
        if (objects != null)
        {
            foreach (var obj in objects)
            {
                DestroyImmediate(obj as Object);
            }
        }
    }
}
#endif
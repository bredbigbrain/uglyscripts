﻿using System.Collections.Generic;
using UnityEngine;

public class PlayersButtons : ScriptableObject
{
    [SerializeField]
    public List<ItemWhithProperties> buttonsWithProperties = new List<ItemWhithProperties>();

    [SerializeField]
    public bool setMapMark = true;
    [SerializeField]
    public string mapMarkName = "";
    [SerializeField]
    public bool? mapMarkFound = null;
}

[System.Serializable]
public class ItemWhithProperties
{
    public string abilityType = "";
    public FoundState abilityFound = FoundState.none;
    public List<string> properties = new List<string>() { "" };
    public List<string> objects = new List<string>() { "" };
    public List<bool> enabled = new List<bool>() { true };
    public List<FoundState> found = new List<FoundState>() { FoundState.none };

    public enum FoundState : int
    {
        found, notFound, none
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System;
using System.Reflection;

public class PlayerButtonsSetter : EditorWindow
{
    [MenuItem("Animals/Ugly tools/Set players buttons")]
    static void Init()
    {
        PlayerButtonsSetter window = (PlayerButtonsSetter)GetWindow(typeof(PlayerButtonsSetter));

        if (window.pButtons == null)
        {
            window.pButtons = AssetDatabase.LoadAssetAtPath<PlayersButtons>("Assets/Scripts/UglyScripts/PlayersButtons.asset");
        }
        foreach (var item in window.pButtons.buttonsWithProperties)
        {
            if (item.found != null)
            {
                item.abilityFound = ItemWhithProperties.FoundState.none;
                for (int i = 0; i < item.found.Count; i++)
                {
                    item.found[i] = ItemWhithProperties.FoundState.none;
                }
            }
        }
        window.pButtons.mapMarkFound = null;

        window.minSize = new Vector2(430, 250);
        window.maxSize = new Vector2(430, 4000);

        if (window.iconsFolder != null)
        {
            window.GetFilesInFolder();
        }
        window.Show();
    }

    [HideInInspector]
    public PlayersButtons pButtons;
    public UnityEngine.Object iconsFolder;
    private bool findDisabledButtons = true, logErrors = false;
    string[] aMaterialFiles;
    Transform uiRoot, player;
    Transform[] transforms;

    void OnGUI()
    {
        EditorGUILayout.LabelField("Icons folder");
        iconsFolder = EditorGUILayout.ObjectField("", iconsFolder, typeof(UnityEngine.Object), false);
        GUILayout.Space(3);

        if (pButtons.buttonsWithProperties.Count != 0)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Ability type", GUILayout.Width(130));
            EditorGUILayout.LabelField("Field name", GUILayout.Width(100));
            EditorGUILayout.LabelField("Target GO name", GUILayout.Width(100));
            EditorGUILayout.LabelField("Set", GUILayout.Width(100));
            EditorGUILayout.EndHorizontal();
        }
        for (int i = 0; i < pButtons.buttonsWithProperties.Count; i++)
        {
            if (pButtons.buttonsWithProperties[i].abilityFound != ItemWhithProperties.FoundState.none)
            {
                GUI.backgroundColor = pButtons.buttonsWithProperties[i].abilityFound == ItemWhithProperties.FoundState.found ? new Color(0.75f, 1, 0.75f) : new Color(1, 0.75f, 0.75f);
            }
            else
            {
                GUI.backgroundColor = Color.white;
            }
            EditorGUILayout.BeginHorizontal();
            pButtons.buttonsWithProperties[i].abilityType = EditorGUILayout.TextField("", pButtons.buttonsWithProperties[i].abilityType, GUILayout.Width(130));
            EditorGUILayout.BeginVertical();
            for (int j = 0; j < pButtons.buttonsWithProperties[i].properties.Count; j++)
            {
                if (!pButtons.buttonsWithProperties[i].enabled[j])
                {
                    pButtons.buttonsWithProperties[i].found[j] = ItemWhithProperties.FoundState.none;
                }
                if (pButtons.buttonsWithProperties[i].found[j] != ItemWhithProperties.FoundState.none)
                {
                    GUI.backgroundColor = pButtons.buttonsWithProperties[i].found[j] == ItemWhithProperties.FoundState.found ? new Color(0.75f, 1, 0.75f) : new Color(1, 0.75f, 0.75f);
                }
                else
                {
                    GUI.backgroundColor = Color.white;
                }
                EditorGUILayout.BeginHorizontal();
                pButtons.buttonsWithProperties[i].properties[j] = EditorGUILayout.TextField("", pButtons.buttonsWithProperties[i].properties[j], GUILayout.Width(100));
                pButtons.buttonsWithProperties[i].objects[j] = EditorGUILayout.TextField("", pButtons.buttonsWithProperties[i].objects[j], GUILayout.Width(100));
                pButtons.buttonsWithProperties[i].enabled[j] = EditorGUILayout.Toggle(pButtons.buttonsWithProperties[i].enabled[j], GUILayout.Width(20));

                if (GUILayout.Button("-", GUILayout.Width(15), GUILayout.Height(15)))
                {
                    pButtons.buttonsWithProperties[i].properties.RemoveAt(j);
                    pButtons.buttonsWithProperties[i].objects.RemoveAt(j);
                    pButtons.buttonsWithProperties[i].enabled.RemoveAt(j);
                    pButtons.buttonsWithProperties[i].found.RemoveAt(j);

                    if (pButtons.buttonsWithProperties[i].properties.Count == 0)
                    {
                        pButtons.buttonsWithProperties.RemoveAt(i);
                    }
                    EditorUtility.SetDirty(pButtons);
                    EditorGUILayout.EndHorizontal();
                    return;
                };
                
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndVertical();
            GUI.backgroundColor = Color.white;
            if (GUILayout.Button("+", GUILayout.Width(15), GUILayout.Height(15)))
                {
                    pButtons.buttonsWithProperties[i].properties.Add("");
                    pButtons.buttonsWithProperties[i].objects.Add("");
                    pButtons.buttonsWithProperties[i].enabled.Add(true);
                    pButtons.buttonsWithProperties[i].found.Add(ItemWhithProperties.FoundState.none);

                    EditorUtility.SetDirty(pButtons);
                    EditorGUILayout.EndHorizontal();
                    return;
                };
            if (GUILayout.Button("X", GUILayout.Width(20), GUILayout.Height(18 * pButtons.buttonsWithProperties[i].properties.Count)))
            {
                pButtons.buttonsWithProperties.RemoveAt(i);
                continue;
            };
            EditorGUILayout.EndHorizontal();
        }
        GUI.backgroundColor = Color.white;
        if (GUILayout.Button("Add ability", GUILayout.Width(105f), GUILayout.Height(20f)))
        {
            try
            {
                pButtons.buttonsWithProperties.Add(new ItemWhithProperties());
            }
            catch (System.Exception ex)
            {
                if (logErrors)
                {
                    Debug.LogError(ex, this);
                }
            }
        };

        pButtons.setMapMark = EditorGUILayout.BeginToggleGroup("Set map mark", pButtons.setMapMark);
        GUI.backgroundColor = pButtons.mapMarkFound == true ? new Color(0.75f, 1, 0.75f) : new Color(1, 0.75f, 0.75f);
        if (pButtons.mapMarkFound == null)
        {
            GUI.backgroundColor = Color.white;
        }
        pButtons.mapMarkName = EditorGUILayout.TextField("", pButtons.mapMarkName, GUILayout.Width(100));
        EditorGUILayout.EndToggleGroup();

        GUILayout.Space(3);
        GUI.backgroundColor = Color.white;
        findDisabledButtons = EditorGUILayout.Toggle("Find disabled buttons", findDisabledButtons);
        logErrors = EditorGUILayout.Toggle("Log errors", logErrors);

        GUILayout.Space(3);
        if (GUILayout.Button("Set", GUILayout.Height(30f)))
        {
            SetPlayerMapMark();
            SetButtons();
        }

        EditorUtility.SetDirty(pButtons);
    }

    void SetButtons()
    {
        try
        {
            if (!player)
            {
                player = GameObject.FindObjectOfType<Player>().transform.Find("Abilities");
            }
            if (!uiRoot)
            {
                uiRoot = GameObject.Find("CanvasGame").transform.Find("GameGUI").Find("Controls").transform;
            }

            if (!player || !uiRoot)
            {
                foreach (var item in pButtons.buttonsWithProperties)
                {
                    for (int i = 0; i < item.found.Count; i++)
                    {
                        item.found[i] = ItemWhithProperties.FoundState.notFound;
                        item.abilityFound = ItemWhithProperties.FoundState.notFound;
                    }
                }
                pButtons.mapMarkFound = false;
                return;
            }

            transforms = uiRoot.GetComponentsInChildren<Transform>(findDisabledButtons);

            foreach (var button in pButtons.buttonsWithProperties)
            {
                try
                {
                    object ability = new object();
                    try
                    {
                        Type type = Type.GetType(button.abilityType);
                        ability = player.GetComponentInChildren(type, true);
                        button.abilityFound = ItemWhithProperties.FoundState.found;
                    }
                    catch (Exception ex)
                    {

                        button.abilityFound = ItemWhithProperties.FoundState.notFound;
                        for (int i = 0; i < button.found.Count; i++)
                        {
                            button.found[i] = ItemWhithProperties.FoundState.notFound;
                        }

                        if (logErrors)
                        {
                            Debug.LogError(ex, this);
                        }
                        continue;
                    }
                    for (int i = 0; i < button.properties.Count; i++)
                    {
                        try
                        {
                            if (button.enabled[i])
                            {
                                if (button.objects[i] == "")
                                {
                                    button.found[i] = ItemWhithProperties.FoundState.notFound;
                                    continue;
                                }
                                var target = FindGO(button.objects[i]);
                                var sprite = FindSpriteInFolder(button.objects[i]);

                                if (!target && !sprite)
                                {
                                    button.found[i] = ItemWhithProperties.FoundState.notFound;
                                    continue;
                                }

                                var fields = ability.GetType().GetFields();

                                foreach (var field in fields)
                                {
                                    if (field.Name == button.properties[i])
                                    {
                                        Component value;
                                        if (target)
                                        {
                                            value = target.GetComponent(field.FieldType);
                                            field.SetValue(ability, value);
                                        }
                                        else
                                        {
                                            field.SetValue(ability, sprite);
                                        }
                                        button.found[i] = ItemWhithProperties.FoundState.found;
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            button.found[i] = ItemWhithProperties.FoundState.notFound;
                            if (logErrors)
                            {
                                Debug.LogError(ex, this);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    for (int i = 0; i < button.found.Count; i++)
                    {
                        button.found[i] = ItemWhithProperties.FoundState.notFound;
                    }
                    if (logErrors)
                    {
                        Debug.LogError(ex, this);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            foreach (var item in pButtons.buttonsWithProperties)
            {
                for (int i = 0; i < item.found.Count; i++)
                {
                    item.found[i] = ItemWhithProperties.FoundState.notFound;
                    item.abilityFound = ItemWhithProperties.FoundState.notFound;
                }
            }
            pButtons.mapMarkFound = false;
            if (logErrors)
            {
                Debug.LogError(ex, this);
            }
        }
    }

    void SetPlayerMapMark()
    {
        if (pButtons.setMapMark)
        {
            GameObject mapMark = GameObject.Find(pButtons.mapMarkName);
            if (mapMark == null)
            {
                pButtons.mapMarkFound = false;
            }
            else
            {
                pButtons.mapMarkFound = true;
                var pl = FindObjectOfType<Player>().mapMark = mapMark;
            }
        }
    }

    Transform FindGO(string name)
    {
        foreach (var trans in transforms)
        {
            if (trans.name.Equals(name))
            {
                return trans;
            }
        }

        return null;
    }

    void GetFilesInFolder()
    {
        var tepm = (AssetDatabase.GetAssetPath(iconsFolder) + "/").Replace("Assets", "");
        aMaterialFiles = System.IO.Directory.GetFiles(Application.dataPath + tepm);
    }

    Sprite FindSpriteInFolder(string name)
    {
        if (iconsFolder != null)
        {
            if (aMaterialFiles == null || aMaterialFiles.Length == 0)
            {
                var tepm = (AssetDatabase.GetAssetPath(iconsFolder) + "/").Replace("Assets", "");
                aMaterialFiles = System.IO.Directory.GetFiles(Application.dataPath + tepm);
            }
            foreach (string matFile in aMaterialFiles)
            {
                if (!matFile.ToLower().Contains(".meta"))
                {
                    string assetPath = "Assets" + matFile.Replace(Application.dataPath, "").Replace('\\', '/');
                    Sprite sprite = (Sprite)AssetDatabase.LoadAssetAtPath(assetPath, typeof(Sprite));

                    if (sprite != null && sprite.name == name)
                    {
                        return sprite;
                    }
                }
            }
        }
        return null;
    }
}
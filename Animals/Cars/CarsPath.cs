﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class CarsPath : MonoBehaviour {
    
    [Header("Execute In EditMode")]
    public Transform emptyTransformsContainer;
    private Transform[] transforms;

    public float maxDistanceToPoint = 10f;

    public bool useRaycast = true;
    public LayerMask excludingMask;

    public bool scan_button = false;
    public bool changeSelectedDirections_button = false;
    public bool save_button = false;
    public bool loadFromManager_button = false;
    public bool clearScene_button = false;
    public bool addIndexToPointsNames_button = false;

    public WayPoint[] wayPoints;

    public Mesh directionMesh;
    public Vector3 meshRotationOffset;

    private Transform lastContainer;
    private bool drawLines = false;
    /*
    void Update ()
    {
        if (scan_button)
        {
            Scan();
            scan_button = false;
        }
        else if (clearScene_button)
        {
            ClearScene();
        }
        else if (changeSelectedDirections_button)
        {
            ChangeDirections();
            changeSelectedDirections_button = false;
        }
        else if (save_button)
        {
            Save();
            save_button = false;
        }
        else if (loadFromManager_button)
        {
            Load();
            loadFromManager_button = false;
        }
        else if (addIndexToPointsNames_button)
        {
            AddPostfixes();
            addIndexToPointsNames_button = false;
        }
	}
    */
    public void Scan()
    {
        if (emptyTransformsContainer == null)
        {
            return;
        }
        var points = emptyTransformsContainer.GetComponentsInChildren<WayPoint>(true);

        foreach (var point in points)
        {
            DestroyImmediate(point);
        }

        wayPoints = new WayPoint[transforms.Length];
        for (int i = 0; i < transforms.Length; i++)
        {
            wayPoints[i] = transforms[i].gameObject.AddComponent<WayPoint>();
        }

        for (int i = 0; i < transforms.Length; i++)
        {
            for (int j = 0; j < transforms.Length; j++)
            {
                if (j != i)
                {
                    float distance = (transforms[i].position - transforms[j].position).magnitude;
                    if (distance <= maxDistanceToPoint)
                    {
                        if (useRaycast)
                        {
                            var direction = transforms[j].transform.position - transforms[i].position;
                            if (Physics.Raycast(transforms[i].position, direction, maxDistanceToPoint, excludingMask))
                            {
                                continue;
                            }
                        }
                        if (!wayPoints[i].incomingWays.Contains(wayPoints[j]))
                        {
                            wayPoints[i].outomingWays.Add(wayPoints[j]);
                            wayPoints[i].distances.Add(distance);
                            wayPoints[j].incomingWays.Add(wayPoints[i]);
                        }
                    }
                }
            }
        }

        foreach (var point in wayPoints)
        {
            point.TrySwapDirection();
        }
    }

    public void ChangeDirections()
    {
        var selectecion = Selection.objects;
        List<WayPoint> points = new List<WayPoint>();
        foreach (var selected in selectecion)
        {
            GameObject go = selected as GameObject;
            if (go != null)
            {
                var point = go.GetComponent<WayPoint>();
                if (point != null)
                {
                    if (!point.ForceSwap())
                    {
                        point.TrySwapDirection();
                    }
                }
            }
        }
    }

    public void Save()
    {
        var carsManager = FindObjectOfType<CarsManager>();
        if (carsManager == null)
        {
            GameObject go = new GameObject("CarsManager");
            carsManager = go.AddComponent<CarsManager>();
        }
        carsManager.wayPoints = wayPoints;
        carsManager.useRaycast = useRaycast;
        carsManager.mesh = directionMesh;
        carsManager.meshOffset = meshRotationOffset;
        carsManager.mask = excludingMask;
        carsManager.maxDistance = maxDistanceToPoint;
        carsManager.container = emptyTransformsContainer;
    }

    public void Load()
    {
        var carsManager = FindObjectOfType<CarsManager>();
        if (carsManager != null && carsManager.wayPoints.Length > 0)
        {
            wayPoints = carsManager.wayPoints;
            useRaycast = carsManager.useRaycast;
            meshRotationOffset = carsManager.meshOffset;
            maxDistanceToPoint = carsManager.maxDistance;
            directionMesh = carsManager.mesh;
            emptyTransformsContainer = carsManager.container;
            excludingMask = carsManager.mask;
        }
    }

    public void AddPostfixes()
    {
        for (int i = 0; i < wayPoints.Length; i++)
        {
            wayPoints[i].name += "_" + i;
        }
    }

    public void ClearScene()
    {
        wayPoints = null;
        transforms = null;
        emptyTransformsContainer = null;
        lastContainer = null;

        var points = FindObjectsOfType<WayPoint>();
        foreach (var point in points)
        {
            DestroyImmediate(point);
        }
        clearScene_button = false;
    }

    private void OnValidate()
    {
        if (emptyTransformsContainer != null)
        {
            if (emptyTransformsContainer != lastContainer)
            {
                transforms = emptyTransformsContainer.Childs();
                lastContainer = emptyTransformsContainer;
            }
        }
        else
        {
            lastContainer = null;
            wayPoints = null;
        }
    }
}


public class CarPathGizmoDrawer
{
    [DrawGizmo(GizmoType.Selected | GizmoType.Active | GizmoType.NotInSelectionHierarchy | GizmoType.NonSelected)]
    public static void DrawGizmo(CarsPath cars, GizmoType gizmoType)
    {
        if (cars.wayPoints != null)
        {
            foreach (var point in cars.wayPoints)
            {
                if (point != null && point.outomingWays != null)
                {
                    foreach (var outcome in point.outomingWays)
                    {
                        if (outcome != null)
                        {
                            Gizmos.DrawLine(point.transform.position, outcome.transform.position);
                            if (cars.directionMesh != null)
                            {
                                int i = point.outomingWays.IndexOf(outcome);
                                Vector3 position = Vector3.MoveTowards(point.transform.position, outcome.transform.position, point.distances[i] / 2);
                                var direction = outcome.transform.position - point.transform.position;
                                Quaternion rotation = Quaternion.LookRotation(direction);
                                rotation.eulerAngles += cars.meshRotationOffset;

                                if (point.incomingWays.Count == 0 || outcome.outomingWays.Count == 0)
                                {
                                    Gizmos.DrawWireMesh(cars.directionMesh, -1, position, rotation);
                                    continue;
                                }

                                Gizmos.DrawMesh(cars.directionMesh, -1, position, rotation);
                            }
                        }
                    }
                }
            }
        }
    }
}
#endif
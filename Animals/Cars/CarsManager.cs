﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarsManager : MonoBehaviour {

    public WayPoint[] wayPoints;

    #region CarsPathData
    [HideInInspector]
    public Transform container;
    [HideInInspector]
    public bool useRaycast;
    [HideInInspector]
    public LayerMask mask;
    [HideInInspector]
    public float maxDistance;
    [HideInInspector]
    public Mesh mesh;
    [HideInInspector]
    public Vector3 meshOffset;
    #endregion

    static private CarsManager instance;
    public static CarsManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<CarsManager>();
            }
            return instance;
        }
    }

    public WayPoint GetClosestPoint(Transform trans, bool checkEmpty = false)
    {
        float dist = -1, tempDist;
        int index = 0;
        for (int i = 0; i < wayPoints.Length; i++)
        {
            if (checkEmpty && wayPoints[i].currentCar != null)
            {
                continue;
            }
            tempDist = (trans.position - wayPoints[i].transform.position).magnitude;
            if (dist == -1 || tempDist < dist)
            {
                dist = tempDist;
                index = i;
            }
        }
        return wayPoints[index];
    }
}

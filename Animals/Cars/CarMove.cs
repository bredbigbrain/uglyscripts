﻿using System.Collections;
using UnityEngine;

public class CarMove : MoveAbility
{
    [Header("CarMove")]
    public WayPoint destination;

    public WayPoint lastDestination;
    public float waitCheckRate = 0.5f;
    public float targetReachedOffset = 0.2f;

    private Coroutine checkCoroutine;
    private float noPathTime = 3f;
    private float lastNoPathTime = -1f;

    private Vector3 targetDirection;

    protected override void CastBotMovement()
    {
        if (canWalk)
        {
            if (destination == null)
            {
                destination = CarsManager.Instance.GetClosestPoint(transform, true);

                if ((transform.position - destination.transform.position).magnitude < targetReachedOffset)
                {
                    FindNextPoint();
                }
                destination.currentCar = animal.gameObject;
            }

            if ((transform.position - destination.transform.position).magnitude < targetReachedOffset)
            {
                if (FindNextPoint())
                {
                    destination.currentCar = animal.gameObject;
                    SetMoveDirection();
                }
                else if (checkCoroutine == null)
                {
                    canWalk = false;
                    moveDirection = Vector3.zero;
                    StartCoroutine(CheckCoroutine());
                }
            }
            else
            {
                SetMoveDirection();
            }
        }
    }

    private IEnumerator CheckCoroutine()
    {
        while (canWalk == false)
        {
            foreach (var point in destination.outomingWays)
            {
                if (point.currentCar == null)
                {
                    canWalk = true;
                    break;
                }
            }
            if (!canWalk)
            {
                yield return new WaitForSeconds(waitCheckRate);
            }
        }
        FindNextPoint();
        destination.currentCar = animal.gameObject;

        checkCoroutine = null;
    }

    private bool FindNextPoint()
    {
        var temp = destination.outomingWays.RandomItem();

        if (temp.currentCar == null)
        {
            if (lastDestination != null)
            {
                lastDestination.currentCar = null;
            }
            lastDestination = destination;
            destination = temp;
            return true;
        }
        foreach (var point in destination.outomingWays)
        {
            if (point.currentCar == null)
            {
                if (lastDestination != null)
                {
                    lastDestination.currentCar = null;
                }
                lastDestination = destination;
                destination = point;
                destination.currentCar = animal.gameObject;
                return true;
            }
        }
        return false;
    }

    private void OnDestroy()
    {
        if (destination != null)
        {
            destination.currentCar = null;
        }
        if (lastDestination != null)
        {
            lastDestination.currentCar = null;
        }
    }

    #region Pasta from PathFindAbility

    public void SetMoveDirection()
    {
        var dir = CalculateVelocity();

        moveDirection = transform.InverseTransformDirection(targetDirection).X() + transform.InverseTransformDirection(dir).Z();
        if (destination != null)
        {
            moveDirection += Vector3.up * (destination.transform.position.y - transform.position.y) * 0.2f;
        }
        moveDirection.Normalize();
        moveDirection = moveDirection.XZ();
    }

    protected Vector3 CalculateVelocity()
    {
        var lastPosition = lastDestination == null ? transform.position : lastDestination.transform.position;

        var dir = destination.transform.position - lastPosition;
        var targetPosition = CalculateTargetPoint(transform.position, lastPosition, destination.transform.position);

        dir = targetPosition - transform.position;
        dir.y = 0;
        float targetDist = dir.magnitude;

        float bodyDistance = (transform.position - destination.transform.position).XZ().magnitude;
        float reacherDistance = (transform.position - destination.transform.position).XZ().magnitude;

        float newTargetDist = Mathf.Min(bodyDistance, reacherDistance);

        float slowdown = Mathf.Clamp01(newTargetDist / 0.5f);

        targetDirection = dir;

        var forward = transform.forward;
        float dot = Vector3.Dot(dir.normalized, forward);
        float sp = Mathf.Max(dot, 0) * slowdown;

        if (Time.deltaTime > 0)
        {
            sp = Mathf.Clamp(sp, 0, newTargetDist / (Time.deltaTime * 2));
        }
        return forward * sp;
    }

    protected Vector3 CalculateTargetPoint(Vector3 p, Vector3 a, Vector3 b)
    {
        a.y = p.y;
        b.y = p.y;

        float magn = (a - b).magnitude;
        if (magn == 0)
        {
            return a;
        }
        float closest = Mathf.Clamp01(ClosestPointOnLineFactor(a, b, p));
        var point = (b - a) * closest + a;
        float distance = (point - p).magnitude;

        float lookAhead = Mathf.Clamp(1 - distance, 0.0F, 1);

        float offset = lookAhead / magn;
        offset = Mathf.Clamp(offset + closest, 0.0F, 1.0F);
        return (b - a) * offset + a;
    }

    protected float XZSqrMagnitude(Vector3 a, Vector3 b)
    {
        float dx = b.x - a.x;
        float dz = b.z - a.z;

        return dx * dx + dz * dz;
    }

    public static float ClosestPointOnLineFactor(Vector3 lineStart, Vector3 lineEnd, Vector3 point)
    {
        var dir = lineEnd - lineStart;
        float sqrMagn = dir.sqrMagnitude;

        if (sqrMagn <= 0.000001)
        {
            return 0;
        }
        return Vector3.Dot(point - lineStart, dir) / sqrMagn;
    }

    #endregion Pasta from PathFindAbility
}
﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CarsPath))]
public class CarPathEditor : Editor
{
    SerializedProperty eventsWhitelistProp;
    public CarsPath cars;

    public override void OnInspectorGUI()
    {
        if (cars == null)
        {
            cars = target as CarsPath;
        }

        EditorGUILayout.PropertyField(serializedObject.FindProperty("emptyTransformsContainer"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("maxDistanceToPoint"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("useRaycast"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("excludingMask"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("directionMesh"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("meshRotationOffset"));

        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Scan", GUILayout.Width(50f), GUILayout.Height(20f)))
        {
            cars.Scan();
        };

        if (GUILayout.Button("Save", GUILayout.Width(50f), GUILayout.Height(20f)))
        {
            cars.Save();
        };

        if (GUILayout.Button("Load", GUILayout.Width(50f), GUILayout.Height(20f)))
        {
            cars.Load();
        };

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Clear scene", GUILayout.Width(105f), GUILayout.Height(20f)))
        {
            cars.ClearScene();
        };

        if (GUILayout.Button("Add indexes", GUILayout.Width(105f), GUILayout.Height(20f)))
        {
            cars.AddPostfixes();
        };
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.LabelField("Select WayPoint");
        if (GUILayout.Button("Change direction", GUILayout.Width(120f), GUILayout.Height(20f)))
        {
            cars.ChangeDirections();
        };

        EditorGUILayout.LabelField("WayPoints count: " + (cars.wayPoints == null? "0" : cars.wayPoints.Length.ToString()));

        serializedObject.ApplyModifiedProperties();
    }
}
#endif
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPoint : MonoBehaviour
{
    public List<WayPoint> incomingWays;
    public List<WayPoint> outomingWays;
    public List<float> distances;

    public GameObject currentCar;

    public WayPoint()
    {
        incomingWays = new List<WayPoint>();
        outomingWays = new List<WayPoint>();
        distances = new List<float>();
    }

    public bool TrySwapDirection()
    {
        if (outomingWays.Count == 0 && incomingWays.Count > 1)
        {
            for (int i = 0; i < incomingWays.Count; i++)
            {
                if (incomingWays[i].outomingWays.Count > 1)
                {
                    int index = incomingWays[i].outomingWays.IndexOf(this);

                    outomingWays.Add(incomingWays[i]);
                    distances.Add(incomingWays[i].distances[index]);

                    incomingWays[i].outomingWays.RemoveAt(index);
                    incomingWays[i].distances.RemoveAt(index);
                    incomingWays[i].incomingWays.Add(this);
                    incomingWays.RemoveAt(i);

                    return true;
                }
            }
        }
        return false;
    }

    public bool ForceSwap()
    {
        if (incomingWays.Count > 0 && outomingWays.Count > 0)
        {
            var tempOutcomes = outomingWays.ToArray();
            outomingWays.Clear();

            for (int i = 0; i < incomingWays.Count; i++)
            {
                incomingWays[i].outomingWays.Remove(this);
                incomingWays[i].incomingWays.Add(this);
                incomingWays[i].CalculateDistances();

                outomingWays.Add(incomingWays[i]);
            }
            incomingWays.Clear();

            for (int i = 0; i < tempOutcomes.Length; i++)
            {
                tempOutcomes[i].outomingWays.Add(this);
                tempOutcomes[i].incomingWays.Remove(this);
                tempOutcomes[i].CalculateDistances();

                incomingWays.Add(tempOutcomes[i]);
            }

            CalculateDistances();
            return true;
        }
        return false;
    }

    public void CalculateDistances()
    {
        distances.Clear();
        for (int i = 0; i < outomingWays.Count; i++)
        {
            float distance = (transform.position - outomingWays[i].transform.position).magnitude;
            distances.Add(distance);
        }
    }
}

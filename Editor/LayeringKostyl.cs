﻿using UnityEngine;

[ExecuteInEditMode]
public class LayeringKostyl : MonoBehaviour
{
    public Vector3 maxSize = Vector3.one;
    [Header("localScale.magnitude < maxSize.magnitude")]
    public LayerMask trueLayer;
    public LayerMask falseLayer;
    
    public bool RESET_BUTTON = false;

    void Update()
    {
        if (RESET_BUTTON)
        {
            int l;
            if (transform.localScale.magnitude < maxSize.magnitude)
            {
                l = (int)Mathf.Log(trueLayer, 2f);
                gameObject.layer = l < 0 ? 31 : l;
            }
            else
            {
                l = (int)Mathf.Log(falseLayer, 2f);
                gameObject.layer = l < 0 ? 31 : l;
            }
            RESET_BUTTON = false;
        }
    }
}
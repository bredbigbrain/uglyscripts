﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class ComponentsChanger : MonoBehaviour
{
    public enum ComponentType: int
    {
        Skip,
        Image,
        Text,
        DeleteAllShadows,
        DeleteAllOutlines,
    }
    public enum SelectBy : int
    {
        All, Name, Sprite, ParentName, Text
    }
    public enum TextProperty : int
    {
        All, LocalScale, SizeDelta, Font, FontStyle, FontSize, Color, BestFit, LocalPosition, gameObjectActivity
    }
    public enum ImageProperty : int
    {
        All, LocalScale, SizeDelta, ImageType, Sprite, LocalPosition, gameObjectActivity
    }
    [System.Serializable]
    public class ChangableObject
    {
        [HideInInspector]
        public string name;
        public GameObject templateObject;
        public ComponentType componentType = ComponentType.Skip;

        //[ConditionalHide("componentType", true)]
        public SelectBy selectBy = SelectBy.All;
        public List<TextProperty> textProperties;
        public List<ImageProperty> imageProperties;

        [HideInInspector]
        public Text templateText;
        [HideInInspector]
        public Image templateImage;
        [HideInInspector]
        public Shadow templateShadow;
        [HideInInspector]
        public Outline templateOutline;
    }

    [Header("Установка свойств шаблонного компонента остальным (дочерним this)")]
    public ChangableObject[] templateObjects;

    public bool CHANGE_BUTTON = false;

    private Image[] images;
    private Text[] texts;
    private Shadow[] shadows;
    private Outline[] outlines;
    private int changed = 0;

    private void Update()
    {
        if (CHANGE_BUTTON)
        {
            if (templateObjects.Length > 0)
            {
                images = transform.GetComponentsInChildren<Image>(true);
                texts = transform.GetComponentsInChildren<Text>(true);
                shadows = transform.GetComponentsInChildren<Shadow>(true);
                outlines = transform.GetComponentsInChildren<Outline>(true);

                foreach (var templObj in templateObjects)
                {
                    switch (templObj.componentType)
                    {
                        case ComponentType.Image:
                            {
                                #region ImageChanging
                                templObj.templateImage = templObj.templateObject.GetComponent<Image>();
                                if (templObj.templateImage == null)
                                {
                                    Debug.LogError("Нет Image на объекте!");
                                    continue;
                                }

                                foreach (var image in images)
                                {
                                    if (((image.sprite == templObj.templateImage.sprite && templObj.selectBy == SelectBy.Sprite) ||
                                        (image.name == templObj.templateObject.name && templObj.selectBy == SelectBy.Name) ||
                                        (image.transform.parent.name == templObj.templateObject.transform.parent.name && templObj.selectBy == SelectBy.ParentName) ||
                                        (templObj.selectBy == SelectBy.All)) && templObj.templateImage != image)
                                    {
                                        bool changed_ = false;
                                        if (templObj.imageProperties.Contains(ImageProperty.LocalScale) || templObj.imageProperties.Contains(ImageProperty.All))
                                        {
                                            image.transform.localScale = templObj.templateImage.transform.localScale;
                                            changed_ = true;
                                        }
                                        if (templObj.imageProperties.Contains(ImageProperty.SizeDelta) || templObj.imageProperties.Contains(ImageProperty.All))
                                        {
                                            image.rectTransform.sizeDelta = templObj.templateImage.rectTransform.sizeDelta;
                                            changed_ = true;
                                        }
                                        if (templObj.imageProperties.Contains(ImageProperty.gameObjectActivity) || templObj.imageProperties.Contains(ImageProperty.All))
                                        {
                                            image.gameObject.SetActive(templObj.templateObject.activeSelf);
                                            changed_ = true;
                                        }
                                        if (templObj.imageProperties.Contains(ImageProperty.LocalPosition) || templObj.imageProperties.Contains(ImageProperty.All))
                                        {
                                            image.transform.localPosition = templObj.templateObject.transform.localPosition;
                                            changed_ = true;
                                        }
                                        if (templObj.imageProperties.Contains(ImageProperty.ImageType) || templObj.imageProperties.Contains(ImageProperty.All))
                                        {
                                            image.type = templObj.templateImage.type;
                                            changed_ = true;
                                        }
                                        if (templObj.imageProperties.Contains(ImageProperty.Sprite) || templObj.imageProperties.Contains(ImageProperty.All))
                                        {
                                            image.sprite = templObj.templateImage.sprite;
                                            changed_ = true;
                                        }
                                        if (changed_)
                                        {
                                            changed++;
#if UNITY_EDITOR
                                            EditorUtility.SetDirty(image);
#endif
                                        }
                                    }
                                }
                                #endregion
                                break;
                            }
                        case ComponentType.Text:
                            {
                                #region TextChanging
                                templObj.templateText = templObj.templateObject.GetComponent<Text>();
                                if (templObj.templateText == null)
                                {
                                    Debug.LogError("Нет Text на объекте!");
                                    continue;
                                }
                                foreach (var text in texts)
                                {
                                    if (((text.transform.name == templObj.templateText.transform.name && templObj.selectBy == SelectBy.Name) ||
                                        (text.transform.parent.name == templObj.templateObject.transform.parent.name && templObj.selectBy == SelectBy.ParentName) ||
                                        (text.text == templObj.templateText.text && templObj.selectBy == SelectBy.Text) ||
                                        (templObj.selectBy == SelectBy.All)) && templObj.templateText != text) 
                                    {
                                        bool changed_ = false;
                                        if (templObj.textProperties.Contains(TextProperty.LocalScale) || templObj.textProperties.Contains(TextProperty.All))
                                        {
                                            text.transform.localScale = templObj.templateText.transform.localScale;
                                            changed_ = true;
                                        }
                                        if (templObj.textProperties.Contains(TextProperty.SizeDelta) || templObj.textProperties.Contains(TextProperty.All))
                                        {
                                            text.rectTransform.sizeDelta = templObj.templateText.rectTransform.sizeDelta;
                                            changed_ = true;
                                        }
                                        if (templObj.textProperties.Contains(TextProperty.Font) || templObj.textProperties.Contains(TextProperty.All))
                                        {
                                            text.font = templObj.templateText.font;
                                            changed_ = true;
                                        }
                                        if (templObj.textProperties.Contains(TextProperty.FontStyle) || templObj.textProperties.Contains(TextProperty.All))
                                        {
                                            text.fontStyle = templObj.templateText.fontStyle;
                                            changed_ = true;
                                        }
                                        if (templObj.textProperties.Contains(TextProperty.FontSize) || templObj.textProperties.Contains(TextProperty.All))
                                        {
                                            text.fontSize = templObj.templateText.fontSize;
                                            changed_ = true;
                                        }
                                        if (templObj.textProperties.Contains(TextProperty.Color) || templObj.textProperties.Contains(TextProperty.All))
                                        {
                                            text.color = templObj.templateText.color;
                                            changed_ = true;
                                        }
                                        if (templObj.textProperties.Contains(TextProperty.gameObjectActivity) || templObj.textProperties.Contains(TextProperty.All))
                                        {
                                            text.gameObject.SetActive(templObj.templateObject.activeSelf);
                                            changed_ = true;
                                        }
                                        if (templObj.textProperties.Contains(TextProperty.LocalPosition) || templObj.textProperties.Contains(TextProperty.All))
                                        {
                                            text.transform.localPosition = templObj.templateObject.transform.localPosition;
                                            changed_ = true;
                                        }
                                        if (templObj.textProperties.Contains(TextProperty.BestFit) || templObj.textProperties.Contains(TextProperty.All))
                                        {
                                            text.resizeTextForBestFit = templObj.templateText.resizeTextForBestFit;
                                            text.resizeTextMaxSize = templObj.templateText.resizeTextMaxSize;
                                            text.resizeTextMinSize = templObj.templateText.resizeTextMinSize;
                                            changed_ = true;
                                        }
                                        if (changed_)
                                        {
                                            changed++;
#if UNITY_EDITOR
                                            EditorUtility.SetDirty(text);
#endif
                                        }
                                    }
                                }
                                #endregion
                                break;
                            }
                        case ComponentType.DeleteAllShadows:
                            {
                                foreach (var shadow in shadows)
                                {
                                    templObj.templateObject = null;
                                    DestroyImmediate(shadow);
                                    changed++;
                                }
                                break;
                            }
                        case ComponentType.DeleteAllOutlines:
                            {
                                foreach (var outline in outlines)
                                {
                                    templObj.templateObject = null;
                                    DestroyImmediate(outline);
                                    changed++;
                                }
                                break;
                            }
                        case ComponentType.Skip:
                            {
                                changed = -1;
                                break;
                            }
                    }
                    if (changed >= 0)
                    {
                        if (templObj.templateObject != null)
                        {
                            if (((templObj.componentType == ComponentType.Text && templObj.textProperties.Count == 0) ||
                            (templObj.componentType == ComponentType.Image && templObj.imageProperties.Count == 0)) && changed == 0)
                            {
                                Debug.Log("Не выставлены свойства для изменения (" + templObj.name + ")");
                            }
                            else
                            {
                                Debug.Log(changed == 0 ? "Не найдено схожих объектов c " + templObj.templateObject.name : "Изменено схожих с " + templObj.templateObject.name + ": " + changed);
                            }
                        }
                        else 
                        {
                            Debug.Log(changed == 0 ? "Не найдено нужных объектов" : "Удалено: " + changed + " (" + templObj.componentType.ToString() + ")");
                        }
                    }
                    changed = 0;
                }
            }
            else
            {
                Debug.Log("Нет шаблонных объектов");
            }
            CHANGE_BUTTON = false;
        }
    }

    private void OnValidate()
    {
        if(templateObjects.Length > 0)
        {
            foreach (var templObj in templateObjects)
            {
                if (templObj.templateObject != null)
                {
                    templObj.name = templObj.templateObject.name;
                    if (templObj.componentType == ComponentType.Text)
                    {
                        var t = templObj.templateObject.GetComponent<Text>();
                        if (t == null)
                        {
                            templObj.componentType = ComponentType.Skip;
                        }
                        else if (templObj.selectBy == SelectBy.Sprite)
                        {
                            templObj.selectBy = SelectBy.All;
                        }
                    }
                    if (templObj.componentType == ComponentType.Image)
                    {
                        var i = templObj.templateObject.GetComponent<Image>();
                        if (i == null)
                        {
                            templObj.componentType = ComponentType.Skip;
                        }
                        else if (templObj.selectBy == SelectBy.Text)
                        {
                            templObj.selectBy = SelectBy.All;
                        }
                    }


                    switch (templObj.componentType)
                    {
                        case ComponentType.Image:
                            {
                                templObj.name += " / Image";
                                break;
                            }
                        case ComponentType.Text:
                            {
                                templObj.name += " / Text";

                                break;
                            }
                        case ComponentType.Skip:
                            {
                                templObj.name += " / Skip";
                                break;
                            }
                    }
                    if (templObj.selectBy == SelectBy.ParentName)
                    {
                        templObj.name += " | Parent: " + templObj.templateObject.transform.parent.name;
                    }
                }

                if (templObj.componentType == ComponentType.DeleteAllOutlines || templObj.componentType == ComponentType.DeleteAllShadows)
                {
                    templObj.selectBy = SelectBy.All;
                }
            }
        }
    }
}
